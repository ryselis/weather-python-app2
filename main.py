import json

import requests

with open("settings.json", "r") as settings_file:
    settings = json.load(settings_file)
with open("settings_environment.json") as settings_environment_file:
    env_settings = json.load(settings_environment_file)


def get_current_temperature():
    url = "%(url_base)sdata/2.5/weather?id=593116&appid=%(api_key)s" % {
        "url_base": settings["API_ROOT"],
        "api_key": env_settings["API_KEY"]
    }
    response = requests.get(url)
    return json.loads(response.text)["main"]["temp"]


def main():
    current_temperature = get_current_temperature()
    print(current_temperature)


if __name__ == "__main__":
    main()
